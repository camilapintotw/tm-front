# Hora Segura

# Tabla de contenido
- [Acerca del proyecto](#acerca-del-proyecto)
- [Stack tecnológico](#stack-tecnolgico)
- [Estructura del proyecto](#estructura-del-proyecto)
- [Instalación](#instalacin)
  - [Modo desarrollo](#modo-desarrollo)
    - [Variables de entorno](#variables-de-entorno)
    - [Acuerdos de desarrollo](#acuerdos-de-desarrollo)
  - [Scripts disponibles](#scripts-disponibles)
- [ChakraUI](#chakra-ui)
- [Recursos](#recursos)

# Acerca del proyecto
El programa Hora Segura es una plataforma virtual gratuita que ofrece una línea de ayuda a través\
de un chat a niños, niñas y adolescentes que viven experiencias de violencia basadas en su orientación\
sexual, identidad o expresión de género.

A través de este chat se les ofrece orientación, contención y derivación a tratamientos de salud mental\
o redes de apoyo cercanas. 

# Stack tecnológico
- React versión 18
- Firebase versión 9.6.10
- Chakra UI versión 1.8.8

# Estructura del proyecto
El proyecto sigue la metodología [Atomic Design](https://atomicdesign.bradfrost.com/table-of-contents/) de Brad Frost.\
La cual separa elementos visuales en átomos, moléculas, organismos, plantillas y páginas. 

Dentro de la carpeta `src` encontraremos las siguientes carpetas y sub carpetas:

```
- components
---> atoms
    contiene elementos HTML pequeños, como por ejemplo, botones, inputs, imagen, etc.
    
---> molecules
    contiene grupos de elementos de la interfaz visual, es un conjunto de elementos de HTML o "átomos"
    por ejemplo, header, search bar, etc.
    
---> organisms
    contiene una carpeta para el perfil "usuarie" y "voluntarie" cada una de ellas posee elementos más complejos
    de la interfaz visual, como por ejemplo, una sección de la vista compuesta por varias "moléculas".
    
---> templates
    contiene todos los elementos visuales necesarios para una página específica del proyecto.
    
- context
  contiene un "context" para manejar el usuario de firebase para la validación de SMS.
  
- pages
  contiene una carpeta para el perfil "usuarie" y "voluntarie" cada una de ellas posee las distintas 
  interfaces visuales del proyecto.
```

# Instalación
Clona este repositorio. Necesitarás tener instalado `npm` y `node` de forma global.\
[Leer instrucciones de instalación node y npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

Después de clonar el proyecto, corre el comando `npm install` para instalar los paquetes del proyecto. 

## Modo desarrollo
Para el modo de desarrollo, luego de clonar el repositorio vas a necesitar `firebase tools`.\
[Leer instrucciones para instalar firebase de forma global](https://firebase.google.com/docs/cli)

### Variables de entorno
Para realizar la conexión a firebase necesitas un archivo local .env con los secretos del proyecto.\
Una vez que descargues el archivo .env debes asegurarte que una vez descargado el archivo el nombre sea ``.env``\
luego debes dejar este archivo en la carpeta raíz del proyecto, junto a archivos como `.gitignore` o `package.json`

### Acuerdos de desarrollo
- Debes utilizar elementos visuales de ChakraUI y componentes creados en la carpeta ``atoms`` cuando corresponda usarlos.
- Debes usar la guía de estilos de Figma para seguir la paleta de colores, estilos de texto, medidas, etc.
- Para las medidas de los elementos visuales recomendamos usar la medida [rem](https://franciscoamk.com/unidades-de-medida-en-css/#:~:text=La%20unidad%20de%20medida%20rem,em%20basado%20en%20la%20ra%C3%ADz.) puedes usar [este conversor](https://nekocalc.com/px-to-rem-converter) de px a rem
- Se recomienda trabajar con [trunk-based development](https://dev.to/marianocodes/por-que-trunk-based-development-i5n) es decir, se pueden crear ramas de corta duración que se integran directamente a **main** a través de un pull request.
- Para los commits de git se debe usar la siguiente estructura ``[número-card-trello]nombre-representativo`` por ejemplo, `[134]chat-implementation` puedes escribirlo en español o inglés.
- Recuerda hacer el comentario de tus commits lo más breve y descriptivos posible. 

## Scripts disponibles
En este proyecto puedes correr los siguientes scripts:

### `npm start`

Corre la aplicación en modo desarrollo.\
Abrir [http://localhost:3000](http://localhost:3000) para visualizarla en tu navegador.

La página se volverá a cargar cada vez que hagas cambios. Puedes ver los errores en la consola.

### `npm test`

Lanza los test en modo interactivo.
Revisa la sección de [lanzar tests](https://facebook.github.io/create-react-app/docs/running-tests) para más información.

### `npm run build`

Construye la aplicación para producción en la carpeta `build`.\
Empaqueta correctamente React en modo producción y optimiza la compilación para tener un mejor rendimiento.

La compilación se minimiza y los nombres de archivo incluyen hashes.
¡Tu aplicación está lista para ser deploy!

Ve esta sección acerca de [deployment](https://facebook.github.io/create-react-app/docs/deployment) para más información.


# Chakra UI
En este proyecto se está usando ChakraUI como librería de componentes visuales para React.\
Para conocer más de los componentes que puedes utilizar en el proyecto [revisa la documentación oficial.](https://chakra-ui.com/docs/components)\
Elige el componente más adecuado y sigue los pasos de la pestaña `usage`, **recuerda importar estos elementos**\
al inicio de cada archivo.

# Recursos
[Diseños de UI en Figma](https://www.figma.com/file/j9GohfxmIsmqV7P0FOVwVl/Todo-Mejora?node-id=0%3A1)\
[Tablero de Trello](https://trello.com/invite/b/bQM8jUHL/e4705cd69131333c32145592851dd911/plataforma-todo-mejora)