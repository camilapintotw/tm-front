import { Image } from '@chakra-ui/react';

const TMImage = (props) => {
  return <Image {...props} />;
};

export default TMImage;
