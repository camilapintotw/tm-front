import {
  Container,
  Flex,
  Grid,
  GridItem,
  HStack,
  PinInput,
  PinInputField,
  Text,
} from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';

import ErrorAlert from '../../molecules/ErrorAlert';
import TMButton from '../../atoms/TMButton';
import TMLinkButton from '../../atoms/TMLinkButton';

const PhoneValidation = ({ result }) => {
  const [userPin, setUserPin] = useState('');
  const [validPin, setValidPin] = useState(false);
  const [errorAlert, setErrorAlert] = useState(false);

  const navigate = useNavigate();

  const handlePinChange = (event) => {
    setUserPin(event);
    event.length === 6 ? setValidPin(true) : setValidPin(false);
  };

  const verifyOtpCode = async (e) => {
    e.preventDefault();
    try {
      await result.confirm(userPin);
      navigate('/usuaries/form');
    } catch (error) {
      setErrorAlert(true);
      console.error(error);
    }
  };

  const changePhoneNumber = () => {
    console.log('change phone number');
  };

  const resendCode = () => {
    console.log('resend code');
  };

  const handleButtonColor = () => {
    if (validPin) {
      return '#5C186A';
    } else {
      return '#8E8E8E';
    }
  };

  return (
    <Flex
      w="100%"
      direction="column"
      mt="4.625rem"
      mb="4.625rem"
      alignItems="center"
      boxShadow="md"
    >
      <Container
        maxW="md"
        centerContent
        mt="1.75rem"
      >
        <Text
          color="#5C186A"
          lineHeight="2.125rem"
          fontSize="1.25rem"
          fontStyle="normal"
          fontWeight="600"
        >
          Ingresa el código
        </Text>
      </Container>
      <Container
        centerContent
        mt="3.3125rem"
      >
        <HStack>
          <PinInput
            otp
            type="number"
            onChange={handlePinChange}
            value={userPin}
          >
            <PinInputField />
            <PinInputField />
            <PinInputField />
            <PinInputField />
            <PinInputField />
            <PinInputField />
          </PinInput>
        </HStack>
      </Container>
      <Container
        alignContent="center"
        textAlign="center"
        mt="1.65625rem"
        centerContent
      >
        <TMButton
          bgColor={handleButtonColor}
          textColor="#FFFFFF"
          w="20rem"
          h="2.875rem"
          onClick={verifyOtpCode}
          isDisabled={!validPin}
        >
          Ingresar a la hora segura
        </TMButton>
      </Container>
      <Grid
        templateColumns="repeat(2, 1fr)"
        mt="1.75rem"
        mb="1.75rem"
      >
        <GridItem w="100%">
          <TMLinkButton onClick={changePhoneNumber}>
            Cambiar número
          </TMLinkButton>
        </GridItem>
        <GridItem w="100%">
          <TMLinkButton onClick={resendCode}>No recibí el código</TMLinkButton>
        </GridItem>
      </Grid>
      {errorAlert && (
        <ErrorAlert message="El código de verificación es incorrecto Intentalo nuevamente." />
      )}
    </Flex>
  );
};

export default PhoneValidation;
