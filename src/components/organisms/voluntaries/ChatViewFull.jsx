import { useState } from 'react';
import { ref, onValue } from 'firebase/database';
import { Box, VStack } from '@chakra-ui/react';
import KebabMenu from '../../molecules/KebabMenu';
import VoluntarieChatBottom from '../../molecules/VoluntarieChatBottom';
import { database } from '../../../firebaseConfig.js';
import SentMessage from '../../molecules/SentMessage.jsx';
import { useContext } from 'react';
import { userAuthContext } from '../../../context/UserAuthContext';
import ReceivedMessage from '../../molecules/ReceivedMessage';

const ChatViewFull = () => {
  const [voluntarieChats, setVoluntarieChats] = useState([]);
  const { user } = useContext(userAuthContext);

  const dbVoluntarieChatRef = ref(database, `/chats/`);
  onValue(dbVoluntarieChatRef, (snapshot) => {
    const dataVoluntarieChats = snapshot.val();
    const sentMessages =
      dataVoluntarieChats && Object.values(dataVoluntarieChats);
    if (sentMessages && sentMessages.length !== voluntarieChats.length)
      setVoluntarieChats(sentMessages);
  });
  return (
    <>
      <VStack
        spacing="0"
        w="50%"
        h="100%"
        borderRight="1px"
        borderLeft="1px"
        borderColor="#BEBEBE"
      >
        <KebabMenu />
        <Box
          bg="#E5E5E5"
          h="90%"
          w="100%"
          overflow="auto"
          p="4"
        >
          <SentMessage content="¡Hola! Bienvenide a Hora Segura, un espacio seguro para conversar. Cuéntanos ¿en qué te podemos ayudar hoy? 🥰" />
          {voluntarieChats.map((chat) =>
            user.uid === chat.message?.sender ? (
              <SentMessage
                content={chat.message?.content}
                key={chat.index}
              />
            ) : (
              <ReceivedMessage
                content={chat.message?.content}
                key={chat.index}
              />
            )
          )}
        </Box>
        <VoluntarieChatBottom />
      </VStack>
    </>
  );
};

export default ChatViewFull;
