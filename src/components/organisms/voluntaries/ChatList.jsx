import { Box, Flex, Text, VStack } from '@chakra-ui/react';
import ChatNewMsj from '../../molecules/ChatNewMsg';

const ChatList = (props) => {
  return (
    <Box
      w="25%"
      h="90%"
    >
      <Flex
        h="9%"
        textAlign="start"
        ml="6"
        alignItems="center"
      >
        <Text
          fontStyle="normal"
          fontWeight="800"
        >
          Chats
        </Text>
      </Flex>
      <VStack
        h="90%"
        borderTop="1px"
        borderColor="#BEBEBE"
        overflow="auto"
      >
        <ChatNewMsj {...props} />
      </VStack>
    </Box>
  );
};

export default ChatList;
