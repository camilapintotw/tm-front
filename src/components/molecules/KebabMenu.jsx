import React from 'react';
import { Flex, Text, Button, Box } from '@chakra-ui/react';
import { useBoolean } from '@chakra-ui/react';
import TMImage from '../atoms/TMImage';

const KebabMenu = () => {
  const [kebabMenu, setKebabMenu] = useBoolean();

  return (
    <>
      <Flex
        justifyContent="space-between"
        alignItems="center"
        w="100%"
        h="9.1%"
        borderRight="1px"
        borderTop="1px"
        borderColor="#BEBEBE"
      >
        <Text
          fontStyle="normal"
          fontWeight="800"
          display="flex"
          alignItems="center"
        >
          <TMImage
            src={'/icons/ellipse.svg'}
            m="4"
            w="3"
          />{' '}
          +56 9 12345678
        </Text>
        <Button
          variant={'ghost'}
          bg="white"
          mr="2"
          onClick={setKebabMenu.toggle}
        >
          <TMImage
            src={'/icons/options.svg'}
            w="1.5"
          />
        </Button>
      </Flex>

      {kebabMenu ? (
        <Flex
          justifyContent="space-around"
          h="10%"
          borderTop="1px"
          borderColor="#BEBEBE"
          w="100%"
          mb="-10%"
        >
          <Button bg="white">Chat Crítico</Button>
          <Button bg="white">Traspasar Chat</Button>
          <Button bg="white">Cerrar Chat</Button>
        </Flex>
      ) : null}
    </>
  );
};

export default KebabMenu;
