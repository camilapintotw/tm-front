import {
  Box,
  Flex,
  Popover,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
  Spacer,
  Button,
  HStack,
  Text,
} from '@chakra-ui/react';
import { signOut } from 'firebase/auth';
import 'firebase/compat/auth';
import TMImage from '../atoms/TMImage';
import { auth } from '../../firebaseConfig';
import { useNavigate } from 'react-router-dom';

const VoluntarieChatHeader = () => {
  const navigate = useNavigate();

  const SignOut = async () => {
    try {
      await signOut(auth);
      // eslint-disable-next-line no-undef
      localStorage.clear();
      navigate('/');
    } catch (error) {
      console.log(error);
    }
  };
  // eslint-disable-next-line no-undef
  const voluntarie = JSON.parse(localStorage.getItem('voluntarie'));
  return (
    <Box
      w="100%"
      h="10%"
      borderBottom="1px"
      borderColor="#BEBEBE"
    >
      <Flex>
        <TMImage
          src={'/images/logo-todo-mejora.svg'}
          w={40}
          m="2"
          ml="5"
        />
        <Spacer />
        <Box
          alignItems="center"
          display="flex"
          mr="4"
        >
          <Popover placement="bottom-end">
            <PopoverTrigger>
              <Button
                _hover={{ bg: 'white' }}
                bg="white"
              >
                {voluntarie.email}
                <TMImage
                  ml="2"
                  src="/icons/down.svg"
                />
              </Button>
            </PopoverTrigger>
            <PopoverContent w="150px">
              <PopoverBody textAlign="center">
                <HStack
                  onClick={() => SignOut()}
                  cursor="pointer"
                >
                  <TMImage src="/icons/signout.svg" />
                  <Text>Cerrar Sesión</Text>
                </HStack>
              </PopoverBody>
            </PopoverContent>
          </Popover>
        </Box>
      </Flex>
    </Box>
  );
};

export default VoluntarieChatHeader;
