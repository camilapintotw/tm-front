import { Box, Flex, Text } from '@chakra-ui/react';
import { useRef, useEffect } from 'react';
import '../../App.css';

const SentMessage = ({ content }) => {
  const messageRef = useRef();

  useEffect(() => {
    if (messageRef) messageRef.current.scrollIntoView({ behavior: 'smooth' });
  }, []);

  return (
    <Flex
      alignItems="end"
      direction="column"
    >
      <Box
        mt="0.75rem"
        backgroundColor="#7c4188"
        borderRadius="0.562rem"
        alignItems="end"
        className={'sentMessage'}
      >
        <Text
          color="#ffffff"
          fontSize="0.875rem"
          fontWeight="400"
          m="1rem"
          ref={messageRef}
        >
          {content}
        </Text>
      </Box>
    </Flex>
  );
};

export default SentMessage;
