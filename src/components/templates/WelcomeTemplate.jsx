import { Container, Flex, Text, Heading } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import TMButton from '../atoms/TMButton';

import TMImage from '../atoms/TMImage';

const WelcomeTemplate = () => {
  const navigate = useNavigate();
  const redirectToAuth = () => navigate('/voluntaries/auth');

  return (
    <Flex
      w="100vw"
      h="100vh"
      direction="column"
      alignItems="center"
      justifyItems="center"
      mt="4rem"
    >
      <TMImage
        src={'/images/logo-todo-mejora.svg'}
        alt={'Heart'}
        w={200}
        mb={5}
        mt={5}
      />

      <Heading
        size="md"
        ml="12"
        fontWeight="700"
      >
        Bienvenide Voluntarie
      </Heading>
      <Text
        fontSize="16px"
        fontWeight="400"
        w="391px"
        mt="21px"
        ml="12"
        justifyContent="justify"
      >
        Con nuestro Programa Hora Segura, línea de ayuda gratuita y
        confidencial, por chat atendemos a miles de niñes, niñas, niños,
        adolescentes y a su entorno protector.
      </Text>

      <Container
        mt="-10"
        centerContent
      >
        <TMImage
          src={'/images/family-support.svg'}
          alt={'Family support'}
          mt="-10"
        />
      </Container>

      <TMButton
        bgColor="#5C186A"
        textColor="#FFFFFF"
        w="20rem"
        h="2.875rem"
        mt="4"
        onClick={redirectToAuth}
      >
        Entrar
      </TMButton>
    </Flex>
  );
};

export default WelcomeTemplate;
