import WelcomeTemplate from '../../components/templates/WelcomeTemplate';

const VoluntarieWelcome = () => {
  return <WelcomeTemplate />;
};

export default VoluntarieWelcome;
