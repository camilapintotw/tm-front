import { SET_TEST_STATE } from '../actions/types';

const reducer = (state, action) => {
  switch (action.type) {
    case SET_TEST_STATE:
      return {
        ...state,
        someValue: action.params.newState,
      };
    default:
      return state;
  }
};

export default reducer;
