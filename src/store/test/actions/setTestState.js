import { useDispatch } from '../context';
import { SET_TEST_STATE } from './types';

const setTestState = (newState) => {
  const dispatch = useDispatch();

  dispatch({ type: SET_TEST_STATE, params: { newState } });
};

export default setTestState;
